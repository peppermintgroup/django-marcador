from django.conf.urls import url
from marcador import views as marcador_views

urlpatterns = [
    url(r'^user/(?P<username>[-\w]+)/$', marcador_views.bookmark_user,
        name='marcador_bookmark_user'),
    url(r'^create/$', marcador_views.bookmark_create,
        name='marcador_bookmark_create'),
    url(r'^edit/(?P<pk>\d+)/$', marcador_views.bookmark_edit,
        name='marcador_bookmark_edit'),
    url(r'^$', marcador_views.bookmark_list, name='marcador_bookmark_list'),
]